/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef OWlib_h_
#define OWlib_h_
#include "capi324v221.h"

#define OW_CMD_CONVERTTEMP 0x44
#define OW_CMD_RSCRATCHPAD 0xbe
#define OW_CMD_WSCRATCHPAD 0x4e
#define OW_CMD_CPYSCRATCHPAD 0x48
#define OW_CMD_RECEEPROM 0xb8
#define OW_CMD_RPWRSUPPLY 0xb4
#define OW_CMD_SEARCHROM 0xf0
#define OW_CMD_READROM 0x33
#define OW_CMD_MATCHROM 0x55
#define OW_CMD_SKIPROM 0xcc
#define OW_CMD_ALARMSEARCH 0xec

#define OW_us(num) (num/(LOOP_CYCLES*.05))
#define LOOP_CYCLES 8 //Number of cycles that the loop takes
#define OW_PORT PORTA
#define OW_DDR DDRA
#define OW_PIN PINA
#define OW_DQ PA3

#define OW_INPUT_MODE() OW_DDR &=~ (1<<OW_DQ)
#define OW_OUTPUT_MODE() OW_DDR |= (1<<OW_DQ)
#define OW_LOW() OW_PORT &= ~(1<<OW_DQ)
#define OW_HIGH() OW_PORT |= (1<<OW_DQ)

#define OW_DECIMAL_STEPS_12BIT 625 //.0625
extern void __OW_write_bit(uint8_t);

extern uint8_t OW_read_byte(void);
extern void OW_write_byte(uint8_t);
extern uint8_t OW_reset(void);

extern uint8_t OW_read_bit(void); // Public to allow a status function without a new function being defined.
extern void OW_delay(uint16_t delay);

#endif
