/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "CEENBoT_OW.h"
inline __attribute__((gnu_inline)) void OW_delay(uint16_t delay) {
	while (delay--)
		asm volatile("nop");
}

uint8_t OW_read_bit(void) {
	uint8_t bit = 0;
	//Pull line low for 1uS
	OW_LOW();
	OW_OUTPUT_MODE();
	OW_delay(OW_us( 1));
	//Release line and wait for 14uS
	OW_INPUT_MODE();
	OW_delay(OW_us( 14));
	//Read line value
	if (OW_PIN & (1 << OW_DQ))
		bit = 1;
	//Wait for 45uS to end and return read value
	OW_delay(OW_us( 45));
	return bit;
}

void __OW_write_bit(uint8_t bit) {
	//Pull line low for 1uS
	OW_LOW();
	OW_OUTPUT_MODE();
	OW_delay(OW_us( 1));
	//If we want to write 1, release the line (if not will keep low)
	if (bit)
		OW_INPUT_MODE();
	//Wait for 60uS and release the line
	OW_delay(OW_us( 60));
	OW_INPUT_MODE();
}

void OW_write_byte(uint8_t byte) {
	uint8_t i = 8;
	while (i--) {
		//Write actual bit and shift one position right to make
		//the next bit ready
		__OW_write_bit(byte & 0x01);
		byte >>= 1;
	}
}
uint8_t OW_reset() {
	uint8_t i;
	//Pull line low and wait for 480uS
	OW_LOW();
	OW_OUTPUT_MODE();
	OW_delay(OW_us(480));

	//Release line and wait for 60uS
	OW_INPUT_MODE();
	OW_delay(OW_us( 60));
	//Store line value and wait until the completion of 480uS period
	i = (OW_PIN & (1 << OW_DQ));
	OW_delay(OW_us( 420 ));
	//Return the value read from the presence pulse (0=OK, 1=WRONG)
	return i;
}
uint8_t OW_read_byte(void) {
	uint8_t i = 8, n = 0;
	while (i--) {
		//Shift one position right and store read value
		n >>= 1;
		n |= (OW_read_bit() << 7);
	}
	return n;
}

